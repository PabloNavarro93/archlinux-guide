# ArchLinux activities guide

---

> ### Table of Contents
>
> 1. [Installation](#)
> 2. [Sudo user creation](#)    
> 3. Network configuration
> 4. Web services installation
> 5. Firewall installation
> 6. System clock configuration
> 7. Modification in the boot system
> 8. SSH installation
> 9. FTP services installation

---

## 1. Installation

For the installation of **`ArchLinux`** O.S. we must go to the official website of [ArchLinux.org](https://www.archlinux.org/download/) and download the LiveCD. Once downloaded we mount the `.iso` in our VM. For this guide we will use a 32GB virtual machine for storage and 2GB of RAM.

When executing the machine, it will show us the system console, the first thing we will do is change the keyboard type in case it does not coincide with the one we have. For this we enter:  
```
# loadkeys es
```
Once configured this, we proceed to realize the disk's partition, this is done with `cfdisk`.
```
# cfdisk
```
The following image shows us how the partitions should look:
![img1](https://bytebucket.org/PabloNavarro93/archlinux-guide/raw/979b267df4ef9ca67e0a6daa0d538a9cbf3e1a13/img/archlinux-img1.png)

[ArchLinux installation guide](https://wiki.archlinux.org/index.php/Installation_guide_(Espa%C3%B1ol))

## 2. Sudo user creation

**Sudo** allows a system administrator to delegate authority to give certain users the ability to run commands as `root`, or another user while providing an audit trail of the commands and their arguments. This is an alternative to `su` for running commands as root.

### User Creation

First we will create a new user for the system. To create it, we will enter in the console:
```
# useradd user_name
```
Then we will assign a password to this user:
```
# passwd user_name
```

### Sudo install and configuration of superuser

Now install the `sudo` package.
```
# packman -S sudo
```
Once installed we edit the file of the following route `/etc/sudoers`.
```
# nano /etc/sudoers
```
Inside the file we enter:
```
user_name ALL=(ALL) ALL
```
Now when we enter with the user entered if we prefix `sudo` we can have the privileges of a superuser.

## 3. Network configuration
Netctl is a tool used to configure and manage network connections through profiles.
Install the `netctl` package
```
# pacman -S netctl
```
A profile can be enabled to start at boot by using: 
```
# netctl enable profile
```
## 4. Web services installation

For the installation of the web server we have several options
```
# pacman -S apache
```
Add the following statement to `/ etc / hosts` (if it does not exist).
```
127.0.0.1  localhost.localdomain   localhost
```
Now edit this route `/etc/rc.conf` and add the following sentence
```
#
# Networking
#
"localhost"
```
Comment a module in the Apache configuration file
```
# nano /etc/httpd/conf/httpd.conf
```
```
LoadModule unique_id_module        modules/mod_unique_id.so
```
```
#LoadModule unique_id_module        modules/mod_unique_id.so
```
## 5. Firewall Installation
To configure the Stateful Firewall we use the iptables command. This consists in generating a series of rules that allow to track the status of the network connections that travel through it, distinguishing which are the legitimate packages, allowing their passage and blocking the others.

## 6. System clock configuration
```
# pacman -S ntp
```
```
# systemctl daemon-reload
```
```
# pacman -S apache
```
```
# systemctl enable ntpd
```
```
# systemctl start ntpd
```
```
# systemctl status ntpd
```
```
# nano /etc/ntp.conf
```
```
# systemctl restart ntpd
```
## 7. Modification in the boot system
## 8. SSH installation
We will install the `openssh` package
```
# pacman -S openssh
```
Now we will edit the `sshd_config`
```
# nano /etc/ssh/sshd_config
```
Now we have started the service
```
# systemctl start sshd
```
If we want `sshd` to start together with the system, we enter this into the console
```
# systemctl enable sshd
```
## 9. FTP services installation
```
# pacman -S vsftpd
```
```
# nano /etc/vsftpd.conf
```
```
# cd /etc/
```
```
# mv vsftpd.conf vsftpd.conf.save
```
```
# nano vsftpd.conf
```
